variable "REGION" {
  type = string
  default = "eu-central-1"
  description = "The AWS region to deploy to"
}

variable "S3_STATE_BUCKET" {
  type = string
  default = "terraform-state-<yourname>"
  description = "The S3 bucket to store the Terraform state in"
}

variable "CLUSTER_NAME" {
  type = string
  default = "qdrant-dbaas"
  description = "The name of the EKS cluster"
}

variable "INSTANCE_TYPE" {
  type = string
  default = "t3.medium"
  description = "The name of the EKS cluster"
}