terraform {
  backend "s3" {
    bucket = var.S3_STATE_BUCKET
    key    = "terraform.tfstate"
    region = var.REGION
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.47.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~> 3.4.3"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.4.2"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.6.2"
    }

  }

  required_version = "~> 1.3"
}