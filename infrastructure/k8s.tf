
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.5.1"

  cluster_name    = local.cluster_name
  cluster_version = "1.24"

  vpc_id                         = module.vpc.vpc_id
  subnet_ids                     = [module.vpc.private_subnets[0]]
  cluster_endpoint_public_access = true

  eks_managed_node_group_defaults = {
    ami_type = "AL2_x86_64"

  }

  eks_managed_node_groups = {
    one = {
      name = "node-group-1"

      instance_types = [var.INSTANCE_TYPE]

      min_size     = 1
      max_size     = 3
      desired_size = 2
    }

    two = {
      name = "node-group-2"

      instance_types = [var.INSTANCE_TYPE]

      min_size     = 1
      max_size     = 2
      desired_size = 1
    }
  }
}

resource "kubernetes_config_map" "basic_config_map" {
  metadata {
    name = "basic-config-map"
  }

  data = {
    cluster_endpoinrt = module.eks.cluster_endpoint
    cluster_id = module.eks.cluster_id
    code_repo = resource.aws_codecommit.codecommit.repository_name
    image_registry = resource.aws_ecr_repository.ecr.repository_name
  }
}

resource "kubernetes_secret" "k8s_secret" {
  metadata {
    name = "basic-auth"
  }

  data = {
    cluster_cert_auth = module.eks.cluster_certificate_authority_data
    password = "P4ssw0rd"
  }

  type = "kubernetes.io/basic-auth"
}