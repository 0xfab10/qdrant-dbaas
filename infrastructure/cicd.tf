resource "aws_codecommit" "codecommit" {
  repository_name = "${var.CLUSTER_NAME}-repo"
  description     = "CodeCommit repository for ${var.CLUSTER_NAME}"

}

resource "aws_codebuild_project" "codebuild" {
  name          = "${var.CLUSTER_NAME}-codebuild"
  description   = "CodeBuild project for ${var.CLUSTER_NAME}"
  build_timeout = "5"
  artifacts {
    type = "NO_ARTIFACTS"
  }
  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = "aws/codebuild/amazonlinux2-x86_64-standard:3.0"
    type                        = "LINUX_CONTAINER"
    privileged_mode             = true
    image_pull_credentials_type = "CODEBUILD"
  }
  source {
    type            = "CODECOMMIT"
    location        = aws_codecommit.codecommit.clone_url_http
    buildspec       = "buildspec.yml"
    git_clone_depth = 1
  }
  tags = {
    Name = "${var.CLUSTER_NAME}"
  }
}