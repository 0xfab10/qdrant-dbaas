from fastapi import Request, FastAPI, HTTPException
import pika
import json
import time


app = FastAPI()



# Services
async def start_create_instance(num_nodes, compute_resources, user):
  body = {
    "num_nodes": num_nodes,
    "compute_resources": compute_resources,
    "user": user,
    "timestamp": time.time()

  }
  channel.basic_publish(exchange='', routing_key=create_queue_name, body=json.dumps(body))



async def get_job_status(job_id):
  # get job status
  # return job status
  return 1

async def start_delete_instance(instance_id):
  # start delete instance job
  # return job id
  return 1

async def get_user(token):
  return {
    "id": 1,
    "name": "test"
  }
  


# Init
rabbitmq_host = config_map["rabbitmq_host"] or "rabbitmq"


connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host))
channel = connection.channel()

queue_name = config_map["queue_name"] or "qdrant-jobs"
create_queue_name = f"{queue_name}-create"
delete_queue_name = f"{queue_name}-delete"
channel.queue_declare(queue=create_queue_name)
channel.queue_declare(queue=delete_queue_name)




# Routes

@app.post("/create-instance")
async def create_instance(request: Request):
# get post arguments
  body = await request.json()
  token = body['token']
  if (token is None):
    raise HTTPException(status_code=403, detail="token is required")
  user = await get_user(token)
  num_nodes = body['num_nodes']
  if (num_nodes is None):
    raise HTTPException(status_code=400, detail="num_nodes is required")
  compute_resources = body['compute_resources']
  if (compute_resources is None):
    raise HTTPException(status_code=400, detail="compute_resources is required")

  
  
  result = await start_create_instance(num_nodes, compute_resources, user)


  if (result is None or result['job_id'] is None):
    raise HTTPException(status_code=500, detail="Failed to start create instance job")
  return result


@app.get("/get-job")
async def get_job(job_id: int):
  if (job_id is None):
    raise HTTPException(status_code=400, detail="job_id is required")
  
  result = await get_job_status(job_id)

  return result



# Route to delete a qdrant instance (by id)
@app.delete("/delete-instance")
async def delete_instance(request: Request):
  # get post arguments
  body = await request.json()
  instance_id = body['instance_id']
  return await start_delete_instance(instance_id)