import pika, sys, os

#import library to install helm charts


config_map = {}
rabbitmq_host = config_map["rabbitmq_host"] or "rabbitmq"


connection = pika.BlockingConnection(pika.ConnectionParameters(rabbitmq_host))
channel = connection.channel()

queue_name = config_map["queue_name"] or "qdrant-jobs"
create_queue_name = f"{queue_name}-create"
delete_queue_name = f"{queue_name}-delete"
channel.queue_declare(queue=create_queue_name)
channel.queue_declare(queue=delete_queue_name)




def main():

  def create_command(body):
    replica_count = body["replica_count"]
    if replica_count is None:
      raise Exception("replica_count is required")
    user = body["user"]
    if user is None:
      raise Exception("user is required")
    compute_resources = body["compute_resources"]
    # if compute_resources is None:
    #   raise Exception("compute_resources is required")


    return f"helm repo add qdrant https://qdrant.to/helm && helm install qdrant-${user.id}-${user.timestamp} --set replicaCount={replica_count} qdrant/qdrant"


    



  def create_callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    ch.basic_ack(delivery_tag = method.delivery_tag)
    commandTxt = create_command(body)
    os.system(commandTxt)
  

  def delete_callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    ch.basic_ack(delivery_tag = method.delivery_tag)


  channel.basic_consume(queue=create_queue_name, on_message_callback=create_callback)


  channel.basic_consume(queue=delete_queue_name, on_message_callback=delete_callback)

  channel.start_consuming()

if __main__ == "__main__":
  try:
    main()
  except KeyboardInterrupt:
    print("Interrupted")
    try:
      sys.exit(0)
    except SystemExit:
      os._exit(0)